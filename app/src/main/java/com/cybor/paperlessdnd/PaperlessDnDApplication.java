package com.cybor.paperlessdnd;

import android.app.Application;

import io.realm.Realm;

public class PaperlessDnDApplication extends Application
{
    @Override
    public void onCreate()
    {
        Realm.init(this);
        super.onCreate();
    }
}
