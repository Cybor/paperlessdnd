package com.cybor.paperlessdnd.ui.adapters;


import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.cybor.paperlessdnd.R;
import com.cybor.paperlessdnd.data.Character;
import com.cybor.paperlessdnd.data.Spell;
import com.cybor.paperlessdnd.ui.SpellActivity;
import com.cybor.paperlessdnd.ui.SpellsActivity;

import java.util.List;

import io.realm.Realm;

import static com.cybor.paperlessdnd.ui.SpellsActivity.SPELL_REQUEST;
import static com.cybor.paperlessdnd.utils.CommonConstants.POSITION;

public class SpellsListAdapter extends ArrayAdapter
{
    private List<Spell> spells;
    private SpellsActivity activity;

    public SpellsListAdapter(SpellsActivity activity, List<Spell> spells)
    {
        super(activity, R.layout.feature_vh);
        this.spells = spells;
        this.activity = activity;
    }

    @NonNull
    @Override
    public View getView(final int position, View view, @NonNull ViewGroup parent)
    {
        if (view == null)
            view = LayoutInflater.from(getContext()).inflate(R.layout.spell_vh, parent, false);//F*CK YEAH! IT DOES WORK!!!
        Spell spell = spells.get(position);
        ((TextView) view.findViewById(R.id.number_tv))
                .setText(String.format("%s", position + 1));
        ((TextView) view.findViewById(R.id.name_tv))
                .setText(spell.getName());
        ((TextView) view.findViewById(R.id.level_tv))
                .setText(String.format("%s", spell.getLevel()));
        ((TextView) view.findViewById(R.id.power_tv))
                .setText(String.format("%s", spell.getPower()));
        ((TextView) view.findViewById(R.id.modifier_tv))
                .setText(String.format("%s%s",
                        spell.getModifier() >= 0 ? "+" : "",
                        spell.getModifier()));
        view.findViewById(R.id.menu_button).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                PopupMenu menu = new PopupMenu(getContext(), view);
                menu.inflate(R.menu.list_item_menu);
                menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener()
                {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem)
                    {
                        Context context = getContext();
                        Realm realm = Realm.getDefaultInstance();
                        switch (menuItem.getItemId())
                        {
                            case R.id.edit_item:
                                activity.startActivityForResult(new Intent(context,
                                                SpellActivity.class)
                                                .putExtra(POSITION, position),
                                        SPELL_REQUEST);
                                break;
                            case R.id.remove_item:
                                final Character character = Character.getInstance(getContext());
                                realm.executeTransaction(new Realm.Transaction()
                                {
                                    @Override
                                    public void execute(Realm realm)
                                    {
                                        character.getSpells().remove(position);
                                    }
                                });
                                activity.updateDisplayData();
                                break;
                        }
                        return true;
                    }
                });
                menu.show();
            }
        });

        return view;
    }

    @Override
    public int getCount()
    {
        return spells == null ? 0 : spells.size();
    }
}

