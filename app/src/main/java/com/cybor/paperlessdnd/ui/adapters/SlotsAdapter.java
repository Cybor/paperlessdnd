package com.cybor.paperlessdnd.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.cybor.paperlessdnd.R;
import com.cybor.paperlessdnd.data.Slot;

import java.util.List;

public class SlotsAdapter extends ArrayAdapter
{
    public static final int ITEM = 0, REMOVE_BUTTON = 1;
    private AdapterView.OnItemClickListener onItemClickListener;
    private AdapterView.OnItemClickListener onRemoveButtonClickListener;
    private List<Slot> slots;

    public SlotsAdapter(Context context,
                        List<Slot> slots,
                        AdapterView.OnItemClickListener onItemClickListener,
                        AdapterView.OnItemClickListener onRemoveButtonClickListener)
    {
        super(context, R.layout.inventory_vh);
        this.onItemClickListener = onItemClickListener;
        this.onRemoveButtonClickListener = onRemoveButtonClickListener;
        this.slots = slots;
    }

    @NonNull
    @Override
    public View getView(final int position, View view, @NonNull final ViewGroup parent)
    {
        if (view == null)
            view = LayoutInflater
                    .from(getContext())
                    .inflate(R.layout.slot_vh, parent, false);
        Slot slot = slots.get(position);
        TextView slotNameTV = (TextView) view.findViewById(R.id.slot_name_tv);
        slotNameTV.setText(slot.getName());

        ((TextView) view.findViewById(R.id.name_tv)).setText(slot.getInventoryObject().getName());
        ((TextView) view.findViewById(R.id.count_tv))
                .setText(String.format("%s", slot.getInventoryObject().getCount()));

        slotNameTV.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                onItemClickListener.onItemClick(null, view, position, ITEM);
            }
        });
        view.findViewById(R.id.remove_button).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                onRemoveButtonClickListener.onItemClick((ListView) parent,
                        view,
                        position,
                        REMOVE_BUTTON);
            }
        });
        return view;
    }

    @Override
    public int getCount()
    {
        return slots == null ? 0 : slots.size();
    }

}
