package com.cybor.paperlessdnd.ui;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.cybor.paperlessdnd.R;
import com.cybor.paperlessdnd.data.Character;

public class PersonalNotesActivity extends AppCompatActivity
{
    TextView personalNotesTV;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.personal_notes_layout);
        personalNotesTV = (TextView) findViewById(R.id.personal_notes_tv);
        personalNotesTV.setText(Character.getInstance(this).getPersonalNotes());
    }

    @Override
    public void finish()
    {
        Character.getInstance(this).setPersonalNotes(personalNotesTV.getText().toString());
        super.finish();
        overridePendingTransition(R.anim.activity_enter, R.anim.activity_leave);
    }
}
