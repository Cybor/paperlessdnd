package com.cybor.paperlessdnd.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.cybor.paperlessdnd.R;
import com.cybor.paperlessdnd.data.Character;
import com.cybor.paperlessdnd.data.Feature;
import com.cybor.paperlessdnd.data.InventoryObject;
import com.cybor.paperlessdnd.ui.adapters.FeatureListAdapter;
import com.cybor.paperlessdnd.ui.dialogs.TextEditActivity;
import com.cybor.paperlessdnd.utils.CommonConstants;

import java.util.ArrayList;

import io.realm.Realm;

import static android.text.InputType.TYPE_CLASS_NUMBER;
import static com.cybor.paperlessdnd.ui.MainActivity.FIXED;
import static com.cybor.paperlessdnd.ui.dialogs.TextEditActivity.CONTENT_TYPE;
import static com.cybor.paperlessdnd.ui.dialogs.TextEditActivity.DIALOG_CONTENT;
import static com.cybor.paperlessdnd.ui.dialogs.TextEditActivity.DIALOG_TITLE;
import static com.cybor.paperlessdnd.ui.dialogs.TextEditActivity.METADATA;
import static com.cybor.paperlessdnd.ui.dialogs.TextEditActivity.TITLE_EDITABLE;
import static com.cybor.paperlessdnd.utils.CommonConstants.NO_ID;


public class InventoryObjectActivity extends AppCompatActivity implements
        View.OnClickListener,
        AdapterView.OnItemClickListener
{
    private static final int FEATURE_REQUEST = 0;
    private static final String POSITION = "Position";
    private InventoryObject inventoryObject;
    private TextView titleET, countET;
    private Spinner typeSpinner;
    private ListView specificationsLV;
    private boolean editMode;
    private Realm realm;
    private Character character;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inventory_object_layout);
        realm = Realm.getDefaultInstance();
        character = Character.getInstance(this);
        int position = getIntent().getIntExtra(CommonConstants.POSITION, NO_ID);
        editMode = position != NO_ID;
        inventoryObject = editMode ? character.getInventory().get(position) :
                new InventoryObject();

        initViews();
        updateDisplayData();
    }

    private void initViews()
    {
        if (editMode)
        {
            View buttonsPanel = findViewById(R.id.buttons_panel);
            assert buttonsPanel != null;
            ((LinearLayout) buttonsPanel).setWeightSum(3);
            View removeButton = findViewById(R.id.remove_button);
            assert removeButton != null;
            removeButton.setVisibility(View.VISIBLE);
        }

        titleET = (TextView) findViewById(R.id.title_et);
        countET = (TextView) findViewById(R.id.count_et);
        typeSpinner = (Spinner) findViewById(R.id.type_spinner);
        specificationsLV = (ListView) findViewById(R.id.specifications_lv);
        setOnClick(R.id.add_button, R.id.save_button, R.id.remove_button, R.id.cancel_button);
    }

    private void setOnClick(@IdRes int... ids)
    {
        for (int id : ids)
        {
            View view = findViewById(id);
            assert view != null;
            view.setOnClickListener(this);
        }
    }

    private void updateDisplayData()
    {
        if (inventoryObject.getName() != null)
            titleET.setText(inventoryObject.getName());
        countET.setText(String.format("%s", inventoryObject.getCount()));
        typeSpinner.setAdapter(new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1,
                getResources().getStringArray(R.array.inventory_types)));
        typeSpinner.setSelection(inventoryObject.getType());
        specificationsLV.setAdapter(new FeatureListAdapter(this,
                inventoryObject.getSpecifications()));
        specificationsLV.setOnItemClickListener(this);
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.add_button:
                commitData();
                Bundle metadata = new Bundle();
                metadata.putInt(POSITION, NO_ID);
                metadata.putBoolean(FIXED, false);
                startActivityForResult(new Intent(this, TextEditActivity.class)
                                .putExtra(METADATA, metadata)
                                .putExtra(CONTENT_TYPE, TYPE_CLASS_NUMBER)
                                .putExtra(TITLE_EDITABLE, true),
                        FEATURE_REQUEST);
                setResult(RESULT_OK);
                break;
            case R.id.save_button:
                commitData();
                if (!editMode)
                    realm.executeTransaction(new Realm.Transaction()
                    {
                        @Override
                        public void execute(Realm realm)
                        {
                            character.getInventory().add(inventoryObject);
                        }
                    });
                setResult(RESULT_OK);
                finish();
                break;
            case R.id.remove_button:
                if (editMode)
                    realm.executeTransaction(new Realm.Transaction()
                    {
                        @Override
                        public void execute(Realm realm)
                        {
                            inventoryObject.deleteFromRealm();
                        }
                    });
                setResult(RESULT_OK);
                finish();
                break;
            case R.id.cancel_button:
                finish();
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id)
    {
        Feature feature = inventoryObject.getSpecifications().get(position);
        Bundle metadata = new Bundle();
        metadata.putInt(POSITION, position);
        metadata.putBoolean(FIXED, false);
        startActivityForResult(new Intent(this, TextEditActivity.class)
                        .putExtra(DIALOG_TITLE, feature.getName())
                        .putExtra(DIALOG_CONTENT, feature.getFormula())
                        .putExtra(CONTENT_TYPE, TYPE_CLASS_NUMBER)
                        .putExtra(METADATA, metadata)
                        .putExtra(TITLE_EDITABLE, true),
                FEATURE_REQUEST);

    }

    private void commitData()
    {
        realm.executeTransaction(new Realm.Transaction()
        {
            @Override
            public void execute(Realm realm)
            {
                inventoryObject.setCount(Integer.parseInt(countET.getText().toString()));
                inventoryObject.setType(typeSpinner.getSelectedItemPosition());
                inventoryObject.setName(titleET.getText().toString());
                //inventoryObject.setDescription();//TODO:Add me!

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == FEATURE_REQUEST && resultCode == RESULT_OK)
        {
            Bundle metadata = data.getBundleExtra(METADATA);
            final int position = metadata == null ? NO_ID : metadata.getInt(POSITION, NO_ID);
            final Feature feature = new Feature(
                    data.getStringExtra(DIALOG_TITLE),
                    data.getStringExtra(DIALOG_CONTENT),
                    new ArrayList<>(character.getCharacteristics()),
                    true);
            realm.executeTransaction(new Realm.Transaction()
            {
                @Override
                public void execute(Realm realm)
                {
                    if (position == NO_ID)
                    {
                        if (!feature.getName().isEmpty())
                            inventoryObject.getSpecifications().add(feature);
                    } else if (feature.getName().isEmpty())
                        inventoryObject.getSpecifications().remove(position);
                    else
                        inventoryObject.getSpecifications().set(position, feature);
                }
            });
        }
        updateDisplayData();
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void finish()
    {
        super.finish();
        overridePendingTransition(R.anim.activity_enter, R.anim.activity_leave);
    }
}
