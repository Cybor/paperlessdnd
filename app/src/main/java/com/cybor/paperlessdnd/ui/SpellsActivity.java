package com.cybor.paperlessdnd.ui;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.cybor.paperlessdnd.R;
import com.cybor.paperlessdnd.data.Character;
import com.cybor.paperlessdnd.ui.adapters.SpellsListAdapter;

import static com.cybor.paperlessdnd.utils.CommonConstants.POSITION;

public class SpellsActivity extends AppCompatActivity implements
        View.OnClickListener,
        AdapterView.OnItemClickListener
{
    public static final int SPELL_REQUEST = 0;
    private ListView spellsLV;
    private Character character;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.spells_layout);

        character = Character.getInstance(this);

        spellsLV = (ListView) findViewById(R.id.spells_lv);
        assert spellsLV != null;
        spellsLV.setOnItemClickListener(this);

        View addButton = findViewById(R.id.add_button);
        assert addButton != null;
        addButton.setOnClickListener(this);

        updateDisplayData();
    }

    public void updateDisplayData()
    {
        spellsLV.setAdapter(new SpellsListAdapter(this, character.getSpells()));
    }

    @Override
    public void onClick(View view)
    {
        if (view.getId() == R.id.add_button)
            startActivityForResult(new Intent(this, SpellActivity.class), SPELL_REQUEST);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id)
    {
        if (view.getId() == R.id.add_button)
            startActivityForResult(new Intent(this, SpellActivity.class)
                            .putExtra(POSITION, position)
                    , SPELL_REQUEST);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (resultCode == RESULT_OK)
            updateDisplayData();
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void finish()
    {
        super.finish();
        overridePendingTransition(R.anim.activity_enter, R.anim.activity_leave);
    }
}
