package com.cybor.paperlessdnd.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.cybor.paperlessdnd.R;
import com.cybor.paperlessdnd.data.Feature;

import java.util.List;


public class FeatureListAdapter extends ArrayAdapter
{
    private List<Feature> features;

    public FeatureListAdapter(Context context, List<Feature> features)
    {
        super(context, R.layout.feature_vh);
        this.features = features;
    }

    @NonNull
    @Override
    public View getView(int position, View view, @NonNull ViewGroup parent)
    {
        if (view == null)
            view = LayoutInflater.from(getContext()).inflate(R.layout.feature_vh, parent, false);//F*CK YEAH! IT DOES WORK!!!
        ((TextView) view.findViewById(R.id.name_tv)).setText(features.get(position).getName());
        ((TextView) view.findViewById(R.id.value_tv)).setText(String.format("%s", features.get(position).getValue()));
        return view;
    }

    @Override
    public int getCount()
    {
        return features == null ? 0 : features.size();
    }
}
