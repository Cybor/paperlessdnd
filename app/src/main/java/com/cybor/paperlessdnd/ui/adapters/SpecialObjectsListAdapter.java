package com.cybor.paperlessdnd.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.cybor.paperlessdnd.R;
import com.cybor.paperlessdnd.data.InventoryObject;

import java.util.List;

public class SpecialObjectsListAdapter extends ArrayAdapter
{
    public static final int ITEM = 0, REMOVE_BUTTON = 1;
    private AdapterView.OnItemClickListener onItemClickListener;
    private AdapterView.OnItemClickListener onRemoveButtonClickListener;
    private List<InventoryObject> inventory;

    public SpecialObjectsListAdapter(Context context,
                                     List<InventoryObject> inventory,
                                     AdapterView.OnItemClickListener onItemClickListener,
                                     AdapterView.OnItemClickListener onRemoveButtonClickListener)
    {
        super(context, R.layout.inventory_vh);
        this.onItemClickListener = onItemClickListener;
        this.onRemoveButtonClickListener = onRemoveButtonClickListener;
        this.inventory = inventory;
    }

    @NonNull
    @Override
    public View getView(final int position, View view, @NonNull final ViewGroup parent)
    {
        if (view == null)
            view = LayoutInflater
                    .from(getContext())
                    .inflate(R.layout.special_objects_vh, parent, false);
        TextView nameTV = (TextView) view.findViewById(R.id.name_tv);
        nameTV.setText(inventory.get(position).getName());
        nameTV.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                onItemClickListener.onItemClick(null, view, position, ITEM);
            }
        });
        view.findViewById(R.id.remove_button).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                onRemoveButtonClickListener.onItemClick((ListView) parent,
                        view,
                        position,
                        REMOVE_BUTTON);
            }
        });
        return view;
    }

    @Override
    public int getCount()
    {
        return inventory == null ? 0 : inventory.size();
    }

}
