package com.cybor.paperlessdnd.ui.dialogs;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.cybor.paperlessdnd.R;

import static com.cybor.paperlessdnd.ui.dialogs.TextEditActivity.DIALOG_CONTENT;

public class TextSelectActivity extends Activity implements View.OnClickListener
{
    public static final String ITEMS = "Items";
    private Spinner contentSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.text_select_layout);


        contentSpinner = ((Spinner) findViewById(R.id.content_spinner));
        contentSpinner.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, getIntent().getStringArrayExtra(ITEMS)));

        findViewById(R.id.save_button).setOnClickListener(this);
        findViewById(R.id.cancel_button).setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        if (v.getId() == R.id.save_button)
            setResult(RESULT_OK, new Intent().putExtra(DIALOG_CONTENT, contentSpinner.getSelectedItem().toString()));
        finish();
    }
}
