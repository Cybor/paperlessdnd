package com.cybor.paperlessdnd.ui.adapters;


import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.cybor.paperlessdnd.R;
import com.cybor.paperlessdnd.data.Character;
import com.cybor.paperlessdnd.data.Skill;
import com.cybor.paperlessdnd.ui.SkillActivity;
import com.cybor.paperlessdnd.ui.SkillsActivity;

import java.util.List;

import io.realm.Realm;

import static com.cybor.paperlessdnd.ui.SkillsActivity.SKILL_REQUEST;
import static com.cybor.paperlessdnd.utils.CommonConstants.POSITION;

public class SkillsListAdapter extends ArrayAdapter
{
    private List<Skill> skills;
    private SkillsActivity activity;

    public SkillsListAdapter(SkillsActivity activity, List<Skill> skills)
    {
        super(activity, R.layout.feature_vh);
        this.skills = skills;
        this.activity = activity;
    }

    @NonNull
    @Override
    public View getView(final int position, View view, @NonNull ViewGroup parent)
    {
        if (view == null)
            view = LayoutInflater.from(getContext()).inflate(R.layout.skill_vh, parent, false);//F*CK YEAH! IT DOES WORK!!!
        Skill skill = skills.get(position);
        ((TextView) view.findViewById(R.id.number_tv))
                .setText(String.format("%s", position + 1));
        ((TextView) view.findViewById(R.id.name_tv))
                .setText(skill.getName());
        ((TextView) view.findViewById(R.id.upgrade_tv))
                .setText(String.format("%s", skill.getUpgrade()));
        ((TextView) view.findViewById(R.id.bonus_tv))
                .setText(String.format("%s%s",
                        skill.getUpgrade() >= 0 ? "+" : "",
                        skill.getUpgrade()));
        view.findViewById(R.id.menu_button).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                PopupMenu menu = new PopupMenu(getContext(), view);
                menu.inflate(R.menu.list_item_menu);
                menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener()
                {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem)
                    {
                        Context context = getContext();
                        Realm realm = Realm.getDefaultInstance();
                        switch (menuItem.getItemId())
                        {
                            case R.id.edit_item:
                                activity.startActivityForResult(new Intent(context,
                                                SkillActivity.class)
                                                .putExtra(POSITION, position),
                                        SKILL_REQUEST);
                                break;
                            case R.id.remove_item:
                                final Character character = Character.getInstance(getContext());
                                realm.executeTransaction(new Realm.Transaction()
                                {
                                    @Override
                                    public void execute(Realm realm)
                                    {
                                        character.getSkills().remove(position);
                                    }
                                });
                                activity.updateDisplayData();
                                break;
                        }
                        return true;
                    }
                });
                menu.show();
            }
        });

        return view;
    }

    @Override
    public int getCount()
    {
        return skills == null ? 0 : skills.size();
    }

}
