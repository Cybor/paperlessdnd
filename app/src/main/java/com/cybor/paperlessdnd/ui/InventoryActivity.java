package com.cybor.paperlessdnd.ui;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.cybor.paperlessdnd.R;
import com.cybor.paperlessdnd.data.Character;
import com.cybor.paperlessdnd.ui.adapters.InventoryListAdapter;

import static com.cybor.paperlessdnd.utils.CommonConstants.POSITION;

public class InventoryActivity extends AppCompatActivity implements
        View.OnClickListener,
        AdapterView.OnItemClickListener
{
    public static final int INVENTORY_OBJECT_REQUEST = 0;
    private ListView inventoryLV;
    private Character character;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inventory_layout);

        character = Character.getInstance(this);

        inventoryLV = (ListView) findViewById(R.id.inventory_lv);

        View addButton = findViewById(R.id.add_button);
        assert addButton != null;
        addButton.setOnClickListener(this);
        inventoryLV.setOnItemClickListener(this);
        updateDisplayData();
    }

    public void updateDisplayData()
    {
        inventoryLV.setAdapter(new InventoryListAdapter(this, character.getInventory()));
    }

    @Override
    public void onClick(View view)
    {
        if (view.getId() == R.id.add_button)
            startActivityForResult(new Intent(this, InventoryObjectActivity.class),
                    INVENTORY_OBJECT_REQUEST);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        startActivityForResult(new Intent(this, InventoryObjectActivity.class)
                        .putExtra(POSITION, position),
                INVENTORY_OBJECT_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (resultCode == RESULT_OK)
            updateDisplayData();
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void finish()
    {
        super.finish();
        overridePendingTransition(R.anim.activity_enter, R.anim.activity_leave);
    }
}
