package com.cybor.paperlessdnd.ui;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.cybor.paperlessdnd.R;

public class LoadingActivity extends Activity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loading_layout);
        new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                try
                {
                    Thread.sleep(2000);
                } catch (Exception e)
                {
                }
                startActivity(new Intent(LoadingActivity.this, MainActivity.class));
                finish();
            }
        }).start();
    }
}
