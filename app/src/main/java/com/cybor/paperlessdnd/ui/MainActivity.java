package com.cybor.paperlessdnd.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.cybor.paperlessdnd.R;
import com.cybor.paperlessdnd.data.Character;
import com.cybor.paperlessdnd.data.Feature;
import com.cybor.paperlessdnd.ui.adapters.FeatureListAdapter;
import com.cybor.paperlessdnd.ui.dialogs.TextEditActivity;
import com.cybor.paperlessdnd.ui.dialogs.TextSelectActivity;
import com.software.shell.fab.ActionButton;

import java.util.ArrayList;

import io.realm.Realm;

import static android.text.InputType.TYPE_CLASS_NUMBER;
import static android.text.InputType.TYPE_CLASS_TEXT;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.cybor.paperlessdnd.ui.dialogs.TextEditActivity.CONTENT_TYPE;
import static com.cybor.paperlessdnd.ui.dialogs.TextEditActivity.DIALOG_CONTENT;
import static com.cybor.paperlessdnd.ui.dialogs.TextEditActivity.DIALOG_TITLE;
import static com.cybor.paperlessdnd.ui.dialogs.TextEditActivity.METADATA;
import static com.cybor.paperlessdnd.ui.dialogs.TextEditActivity.TIPS;
import static com.cybor.paperlessdnd.ui.dialogs.TextEditActivity.TITLE_EDITABLE;
import static com.cybor.paperlessdnd.ui.dialogs.TextSelectActivity.ITEMS;
import static com.cybor.paperlessdnd.utils.CommonConstants.NO_ID;
import static com.cybor.paperlessdnd.utils.CommonConstants.POSITION;

public class MainActivity extends AppCompatActivity implements
        View.OnClickListener,
        AdapterView.OnItemClickListener
{
    public static final String FIXED = "Fixed";
    private final int TITLE_REQUEST = 0,
            CLASS_REQUEST = 1,
            RACE_REQUEST = 2,
            LEVEL_REQUEST = 3,
            AGE_REQUEST = 4,
            GENDER_REQUEST = 5,
            IDEOLOGY_REQUEST = 6,
            GOD_REQUEST = 7,
            FEATURE_REQUEST = 8;
    private Character character;
    private ListView mainMenu;
    private ActionButton menuButton;
    private ListView fixedCharacteristicsLV, dynamicCharacteristicsLV;
    private TextView titleTV, classTV, raceTV, levelTV, ageTV, genderTV, ideologyTV, godTV;
    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);

        realm = Realm.getDefaultInstance();

        character = Character.getInstance(this);

        initViews();
        initActions();
    }

    private void initViews()
    {
        titleTV = (TextView) findViewById(R.id.title_tv);
        classTV = (TextView) findViewById(R.id.class_tv);
        raceTV = (TextView) findViewById(R.id.race_tv);
        levelTV = (TextView) findViewById(R.id.level_tv);
        ageTV = (TextView) findViewById(R.id.age_tv);
        genderTV = (TextView) findViewById(R.id.gender_tv);
        ideologyTV = (TextView) findViewById(R.id.ideology_tv);
        godTV = (TextView) findViewById(R.id.god_tv);
        fixedCharacteristicsLV = (ListView) findViewById(R.id.fixed_characteristics_lv);
        dynamicCharacteristicsLV = (ListView) findViewById(R.id.dynamic_characteristics_lv);

        mainMenu = (ListView) findViewById(R.id.menu);
        mainMenu.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,
                new String[]
                        {
                                getString(R.string.inventory_label),
                                getString(R.string.clothes_label),
                                getString(R.string.spells_label),
                                getString(R.string.skills_label),
                                getString(R.string.personal_notes_label)
                        }));

        menuButton = (ActionButton) findViewById(R.id.menu_button);

        updateDisplayData();
    }

    private void initActions()
    {
        titleTV.setOnClickListener(this);
        classTV.setOnClickListener(this);
        raceTV.setOnClickListener(this);
        menuButton.setOnClickListener(this);
        fixedCharacteristicsLV.setOnItemClickListener(this);
        dynamicCharacteristicsLV.setOnItemClickListener(this);

        findViewById(R.id.level_tv).setOnClickListener(this);
        findViewById(R.id.age_tv).setOnClickListener(this);
        findViewById(R.id.gender_tv).setOnClickListener(this);
        findViewById(R.id.ideology_tv).setOnClickListener(this);
        findViewById(R.id.god_tv).setOnClickListener(this);
        findViewById(R.id.add_dynamic_characteristic_button).setOnClickListener(this);

        mainMenu.setOnItemClickListener(this);
    }

    private void updateDisplayData()
    {
        if (validateString(character.getName()))
            titleTV.setText(character.getName());
        if (validateString(character.getCharacterClass()))
            classTV.setText(character.getCharacterClass());
        if (validateString(character.getRace()))
            raceTV.setText(character.getRace());
        if (validateString(character.getIdeology()))
            ideologyTV.setText(character.getIdeology());
        if (validateString(character.getGod()))
            godTV.setText(character.getGod());
        levelTV.setText(getString(R.string.level_format).replace("LEVEL", Integer.toString(character.getLevel())));
        ageTV.setText(getString(R.string.age_format).replace("AGE", Integer.toString(character.getAge())));
        genderTV.setText(getString(character.getGender() ? R.string.female_label : R.string.male_label));

        fixedCharacteristicsLV.setAdapter(new FeatureListAdapter(this,
                character.getFixedCharacteristics()));
        dynamicCharacteristicsLV.setAdapter(new FeatureListAdapter(this,
                character.getDynamicCharacteristics(true)));
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event)
    {
        if (keyCode == KeyEvent.KEYCODE_MENU)
            switchMenu();
        return super.onKeyUp(keyCode, event);
    }

    @Override
    public void onBackPressed()
    {
        if (mainMenu.getVisibility() == VISIBLE)
            switchMenu();
        else
            super.onBackPressed();
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.menu_button:
                switchMenu();
                break;
            case R.id.title_tv:
                startActivityForResult(new Intent(this, TextEditActivity.class)
                                .putExtra(DIALOG_TITLE, getString(R.string.character_name_label))
                                .putExtra(DIALOG_CONTENT, ((TextView) findViewById(R.id.title_tv)).getText().toString()),
                        TITLE_REQUEST);
                break;
            case R.id.class_tv:
                startActivityForResult(new Intent(this, TextEditActivity.class)
                                .putExtra(DIALOG_TITLE, getString(R.string.character_class_label))
                                .putExtra(TIPS, getResources().getStringArray(R.array.classes)),
                        CLASS_REQUEST);
                break;
            case R.id.race_tv:
                startActivityForResult(new Intent(this, TextEditActivity.class)
                                .putExtra(DIALOG_TITLE, getString(R.string.character_race_label))
                                .putExtra(TIPS, getResources().getStringArray(R.array.races)),
                        RACE_REQUEST);
                break;
            case R.id.level_tv:
                startActivityForResult(new Intent(this, TextEditActivity.class)
                                .putExtra(DIALOG_TITLE, getString(R.string.level_label))
                                .putExtra(DIALOG_CONTENT, Integer.toString(character.getLevel()))
                                .putExtra(CONTENT_TYPE, TYPE_CLASS_NUMBER),
                        LEVEL_REQUEST);
                break;
            case R.id.age_tv:
                startActivityForResult(new Intent(this, TextEditActivity.class)
                                .putExtra(DIALOG_TITLE, getString(R.string.age_label))
                                .putExtra(DIALOG_CONTENT, Integer.toString(character.getAge()))
                                .putExtra(CONTENT_TYPE, TYPE_CLASS_NUMBER),
                        AGE_REQUEST);
                break;
            case R.id.gender_tv:
                startActivityForResult(new Intent(this, TextSelectActivity.class)
                                .putExtra(DIALOG_TITLE, getString(R.string.gender_label))
                                .putExtra(ITEMS, new String[]{getString(R.string.male_label), getString(R.string.female_label)}),
                        GENDER_REQUEST);
                break;
            case R.id.ideology_tv:
                startActivityForResult(new Intent(this, TextSelectActivity.class)
                                .putExtra(DIALOG_TITLE, getString(R.string.ideology_label))
                                .putExtra(ITEMS, getResources().getStringArray(R.array.ideologies)),
                        IDEOLOGY_REQUEST);
                break;
            case R.id.god_tv:
                startActivityForResult(new Intent(this, TextEditActivity.class)
                                .putExtra(DIALOG_TITLE, getString(R.string.god_label))
                                .putExtra(DIALOG_CONTENT, ((TextView) findViewById(R.id.god_tv))
                                        .getText()
                                        .toString())
                                .putExtra(TIPS, getResources().getStringArray(R.array.gods)),
                        GOD_REQUEST);
                break;
            case R.id.add_dynamic_characteristic_button:
                Bundle metadata = new Bundle();
                metadata.putInt(POSITION, NO_ID);
                metadata.putBoolean(FIXED, false);
                startActivityForResult(new Intent(this, TextEditActivity.class)
                                .putExtra(METADATA, metadata)
                                .putExtra(TITLE_EDITABLE, true),
                        FEATURE_REQUEST);
                break;
        }
    }

    private void switchMenu()
    {
        if (mainMenu.getVisibility() == VISIBLE)
        {
            menuButton.playShowAnimation();
            menuButton.setVisibility(VISIBLE);
            mainMenu.animate().alpha(0);
            mainMenu.setVisibility(GONE);
        } else
        {
            menuButton.playHideAnimation();
            menuButton.setVisibility(GONE);
            mainMenu.animate().alpha(1);
            mainMenu.setVisibility(VISIBLE);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        switch (parent.getId())
        {
            case R.id.menu:
                switch (position)
                {
                    case 0:
                        startActivity(new Intent(this, InventoryActivity.class));
                        break;
                    case 1:
                        startActivity(new Intent(this, ClothesActivity.class));
                        break;
                    case 2:
                        startActivity(new Intent(this, SpellsActivity.class));
                        break;
                    case 3:
                        startActivity(new Intent(this, SkillsActivity.class));
                        break;
                    case 4:
                        startActivity(new Intent(this, PersonalNotesActivity.class));
                        break;
                }
                break;
            case R.id.fixed_characteristics_lv:
            case R.id.dynamic_characteristics_lv:
                boolean fixed = parent.getId() == R.id.fixed_characteristics_lv;
                Feature feature = fixed ?
                        character.getFixedCharacteristics().get(position) :
                        character.getDynamicCharacteristics(false).get(position);
                position = character.getCharacteristics().indexOf(feature);
                Bundle metadata = new Bundle();
                metadata.putInt(POSITION, position);
                metadata.putBoolean(FIXED, fixed);
                startActivityForResult(new Intent(this, TextEditActivity.class)
                                .putExtra(DIALOG_TITLE, feature.getName())
                                .putExtra(DIALOG_CONTENT, feature.getFormula())
                                .putExtra(CONTENT_TYPE, fixed ? TYPE_CLASS_NUMBER : TYPE_CLASS_TEXT)
                                .putExtra(METADATA, metadata)
                                .putExtra(TITLE_EDITABLE, true),
                        FEATURE_REQUEST);
                break;
        }
    }

    private boolean validateString(String string)
    {
        return string != null && !string.trim().replace(" ", "").isEmpty();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (resultCode == RESULT_OK)
        {
            String result = data.getStringExtra(DIALOG_CONTENT);
            switch (requestCode)
            {
                //region Base character information requests
                case TITLE_REQUEST:
                    character.setName(result);
                    break;
                case CLASS_REQUEST:
                    character.setClass(result);
                    break;
                case RACE_REQUEST:
                    character.setRace(result);
                    break;
                case LEVEL_REQUEST:
                    character.setLevel(Integer.parseInt(result));
                    break;
                case AGE_REQUEST:
                    character.setAge(Integer.parseInt(result));
                    break;
                case GENDER_REQUEST:
                    character.setGender(result.equals(getString(R.string.female_label)));
                    break;
                case IDEOLOGY_REQUEST:
                    character.setIdeology(result);
                    break;
                case GOD_REQUEST:
                    character.setGod(result);
                    break;
                //endregion
                case FEATURE_REQUEST:
                    Bundle metadata = data.getBundleExtra(METADATA);
                    final int position = metadata.getInt(POSITION);

                    final Feature feature = new Feature(
                            data.getStringExtra(DIALOG_TITLE),
                            data.getStringExtra(DIALOG_CONTENT),
                            new ArrayList<>(character.getFixedCharacteristics()),
                            metadata.getBoolean(FIXED));
                    realm.executeTransaction(new Realm.Transaction()
                    {
                        @Override
                        public void execute(Realm realm)
                        {
                            if (position == NO_ID)
                            {
                                if (!feature.getName().isEmpty())
                                    character.getCharacteristics().add(feature);
                            } else if (feature.getName().isEmpty())
                                character.getCharacteristics().remove(position);
                            else
                                character.getCharacteristics().set(position, feature);
                        }
                    });
                    break;
            }
            updateDisplayData();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void startActivity(Intent intent)
    {
        super.startActivity(intent);
        overridePendingTransition(R.anim.activity_enter, R.anim.activity_leave);
    }
}