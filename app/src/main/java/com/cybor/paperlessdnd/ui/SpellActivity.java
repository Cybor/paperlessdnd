package com.cybor.paperlessdnd.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.cybor.paperlessdnd.R;
import com.cybor.paperlessdnd.data.Character;
import com.cybor.paperlessdnd.data.Spell;
import com.cybor.paperlessdnd.utils.CommonConstants;

import io.realm.Realm;

import static com.cybor.paperlessdnd.utils.CommonConstants.NO_ID;

public class SpellActivity extends AppCompatActivity
        implements View.OnClickListener
{
    private Realm realm;
    private Character character;
    private Spell spell;
    private boolean editMode;
    private TextView nameET, descriptionET, powerET, levelET, modifierET;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.spell_layout);

        realm = Realm.getDefaultInstance();
        character = Character.getInstance(this);

        int position = getIntent().getIntExtra(CommonConstants.POSITION, NO_ID);
        editMode = position != NO_ID;
        spell = editMode ? character.getSpells().get(position) : new Spell();

        nameET = (TextView) findViewById(R.id.name_et);
        descriptionET = (TextView) findViewById(R.id.description_et);
        powerET = (TextView) findViewById(R.id.power_et);
        levelET = (TextView) findViewById(R.id.level_et);
        modifierET = (TextView) findViewById(R.id.modifier_et);

        if (editMode)
        {
            nameET.setText(spell.getName());
            descriptionET.setText(spell.getDescription());
            powerET.setText(String.format("%s", spell.getPower()));
            levelET.setText(String.format("%s", spell.getLevel()));
            modifierET.setText(String.format("%s", spell.getModifier()));
        }


        View saveButton = findViewById(R.id.save_button);
        View cancelButton = findViewById(R.id.cancel_button);
        assert saveButton != null && cancelButton != null;
        saveButton.setOnClickListener(this);
        cancelButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.save_button:
                realm.executeTransaction(new Realm.Transaction()
                {
                    @Override
                    public void execute(Realm realm)
                    {
                        spell.setName(nameET.getText().toString());
                        spell.setDescription(descriptionET.getText().toString());

                        spell.setPower(Integer.parseInt(powerET.getText().toString()));
                        spell.setLevel(Integer.parseInt(levelET.getText().toString()));
                        spell.setModifier(Integer.parseInt(modifierET.getText().toString()));
                    }
                });

                if (!editMode)
                    realm.executeTransaction(new Realm.Transaction()
                    {
                        @Override
                        public void execute(Realm realm)
                        {
                            character.getSpells().add(spell);
                        }
                    });
                setResult(RESULT_OK);
                finish();
                break;
            case R.id.remove_button:
                if (editMode)
                    realm.executeTransaction(new Realm.Transaction()
                    {
                        @Override
                        public void execute(Realm realm)
                        {
                            spell.deleteFromRealm();
                        }
                    });
                setResult(RESULT_OK);
                finish();
                break;
            case R.id.cancel_button:
                finish();
                break;
        }
    }

    @Override
    public void finish()
    {
        super.finish();
        overridePendingTransition(R.anim.activity_enter, R.anim.activity_leave);
    }
}
