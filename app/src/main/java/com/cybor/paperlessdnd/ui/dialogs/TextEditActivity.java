package com.cybor.paperlessdnd.ui.dialogs;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

import com.cybor.paperlessdnd.R;

import static android.graphics.Color.TRANSPARENT;
import static android.text.InputType.TYPE_CLASS_TEXT;
import static android.view.Window.FEATURE_NO_TITLE;

public class TextEditActivity extends AppCompatActivity implements View.OnClickListener
{
    public static final String DIALOG_TITLE = "DialogTitle",
            DIALOG_CONTENT = "DialogContent",
            TIPS = "Tips",
            CONTENT_TYPE = "ContentType",
            METADATA = "Metadata",
            TITLE_EDITABLE = "TitleEditable";
    private AutoCompleteTextView contentTV;
    private TextView titleTV;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        supportRequestWindowFeature(FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.text_edit_layout);
        getWindow().setBackgroundDrawable(new ColorDrawable(TRANSPARENT));


        titleTV = (TextView) findViewById(getIntent().getBooleanExtra(TITLE_EDITABLE, false) ? R.id.title_et : R.id.title_tv);
        titleTV.setVisibility(View.VISIBLE);
        titleTV.setText(getIntent().getStringExtra(DIALOG_TITLE));

        contentTV = (AutoCompleteTextView) findViewById(R.id.content_tv);
        contentTV.setText(getIntent().getStringExtra(DIALOG_CONTENT));

        contentTV.setInputType(getIntent().getIntExtra(CONTENT_TYPE, TYPE_CLASS_TEXT));

        String[] tips = getIntent().getStringArrayExtra(TIPS);
        if (tips != null)
        {
            contentTV.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, tips));
            contentTV.setOnTouchListener(new View.OnTouchListener()
            {
                @Override
                public boolean onTouch(View v, MotionEvent event)
                {
                    contentTV.showDropDown();
                    return false;
                }
            });
        }

        findViewById(R.id.save_button).setOnClickListener(this);
        findViewById(R.id.cancel_button).setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        if (v.getId() == R.id.save_button)
            setResult(RESULT_OK, new Intent()
                    .putExtra(DIALOG_TITLE, titleTV.getText().toString())
                    .putExtra(DIALOG_CONTENT, contentTV.getText().toString())
                    .putExtra(METADATA, getIntent().getBundleExtra(METADATA)));
        finish();
    }
}
