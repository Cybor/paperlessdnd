package com.cybor.paperlessdnd.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;

import com.cybor.paperlessdnd.R;
import com.cybor.paperlessdnd.data.Character;
import com.cybor.paperlessdnd.data.InventoryObject;
import com.cybor.paperlessdnd.data.Slot;
import com.cybor.paperlessdnd.ui.adapters.SlotsAdapter;
import com.cybor.paperlessdnd.ui.adapters.SpecialObjectsListAdapter;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.cybor.paperlessdnd.data.InventoryObject.SPECIAL_OBJECT;
import static com.cybor.paperlessdnd.data.InventoryObject.TRASH;
import static com.cybor.paperlessdnd.ui.adapters.SpecialObjectsListAdapter.ITEM;
import static com.cybor.paperlessdnd.ui.adapters.SpecialObjectsListAdapter.REMOVE_BUTTON;

public class ClothesActivity extends AppCompatActivity implements
        View.OnClickListener,
        AdapterView.OnItemClickListener
{
    private Character character;
    private EditText addSlotET;
    private Spinner addSpecialObjectSpinner, addSlotSpinner;
    private View addSpecialObjectButton, addSlotButton;
    private ListView specialObjectsLV, slotsLV;
    private RealmList<InventoryObject> inventory;
    private List<InventoryObject> specialObjects;
    private List<InventoryObject> wearObjects;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.clothes_layout);

        character = Character.getInstance(this);
        inventory = character.getInventory();

        initViews();
        updateDisplayData();
    }

    private void initViews()
    {
        addSlotET = (EditText) findViewById(R.id.add_slot_et);
        assert addSlotET != null;
        addSlotSpinner = (Spinner) findViewById(R.id.add_slot_spinner);
        assert addSlotSpinner != null;
        addSlotButton = findViewById(R.id.add_slot_button);
        assert addSlotButton != null;
        addSlotButton.setOnClickListener(this);
        slotsLV = (ListView) findViewById(R.id.slots_lv);
        assert slotsLV != null;

        addSpecialObjectButton = findViewById(R.id.add_special_object_button);
        assert addSpecialObjectButton != null;//It's not NULL! I guarantee that!
        addSpecialObjectButton.setOnClickListener(this);
        addSpecialObjectSpinner = (Spinner) findViewById(R.id.add_special_object_spinner);
        assert addSpecialObjectSpinner != null;
        specialObjectsLV = (ListView) findViewById(R.id.special_objects_lv);
        assert specialObjectsLV != null;
    }

    private void updateDisplayData()
    {
        specialObjects = inventory
                .where()
                .equalTo("type", SPECIAL_OBJECT)
                .findAll();
        if (!specialObjects.isEmpty())
        {
            addSpecialObjectButton.setVisibility(VISIBLE);
            setAdapter(addSpecialObjectSpinner, specialObjects);
        }
        specialObjectsLV.setAdapter(new SpecialObjectsListAdapter(this,
                character.getSpecialObjects(),
                this, this));

        wearObjects = inventory
                .where()
                .notEqualTo("type", TRASH)
                .notEqualTo("type", SPECIAL_OBJECT)
                .findAll();
        if (!wearObjects.isEmpty())
        {
            addSlotButton.setVisibility(VISIBLE);
            setAdapter(addSlotSpinner, wearObjects);
        }
        slotsLV.setAdapter(new SlotsAdapter(this,
                character.getSlots(),
                this, this));
    }

    @Override
    public void onClick(View view)
    {
        if (view == addSpecialObjectButton && specialObjects.size() > 0)
            if (addSpecialObjectSpinner.getVisibility() == VISIBLE)
            {
                addSpecialObjectSpinner.setVisibility(GONE);
                final InventoryObject specialObject =
                        specialObjects.get(addSpecialObjectSpinner.getSelectedItemPosition());
                if (!character.getSpecialObjects().contains(specialObject))
                    Realm.getDefaultInstance().executeTransaction(new Realm.Transaction()
                    {
                        @Override
                        public void execute(Realm realm)
                        {
                            character.getSpecialObjects().add(specialObject);
                        }
                    });
                updateDisplayData();
            } else addSpecialObjectSpinner.setVisibility(VISIBLE);
        else if (view == addSlotButton && wearObjects.size() > 0)
            if (addSlotSpinner.getVisibility() == VISIBLE)
            {
                addSlotET.setVisibility(GONE);
                addSlotSpinner.setVisibility(GONE);
                final InventoryObject wearObject =
                        wearObjects.get(addSlotSpinner.getSelectedItemPosition());
                boolean contains = false;
                for (Slot slot : character.getSlots())
                    contains = contains || slot.getInventoryObject().getName().equals(wearObject.getName());
                if (!contains && addSlotET.getText().length() > 0)
                {
                    final Slot slot = new Slot();
                    slot.setName(addSlotET.getText().toString());
                    slot.setInventoryObject(wearObject);
                    Realm.getDefaultInstance().executeTransaction(new Realm.Transaction()
                    {
                        @Override
                        public void execute(Realm realm)
                        {
                            character.getSlots().add(slot);
                        }
                    });
                }
            } else
            {
                addSlotET.setVisibility(VISIBLE);
                addSlotSpinner.setVisibility(VISIBLE);
            }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, final int position, long id)
    {
        if (adapterView == specialObjectsLV)
            switch ((int) id)
            {
                case ITEM:
                    break;
                case REMOVE_BUTTON:
                    Realm.getDefaultInstance().executeTransaction(new Realm.Transaction()
                    {
                        @Override
                        public void execute(Realm realm)
                        {
                            character.getSpecialObjects().remove(position);
                        }
                    });
                    updateDisplayData();
                    break;
            }
        else if (adapterView == slotsLV)
            switch ((int) id)
            {
                case ITEM:
                    break;
                case REMOVE_BUTTON:
                    Realm.getDefaultInstance().executeTransaction(new Realm.Transaction()
                    {
                        @Override
                        public void execute(Realm realm)
                        {
                            character.getSlots().remove(position);
                        }
                    });
                    updateDisplayData();
                    break;
            }
    }

    private <T> void setAdapter(Spinner spinner, List<T> list)
    {
        spinner.setAdapter(new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item,
                list));
    }

    @Override
    public void finish()
    {
        super.finish();
        overridePendingTransition(R.anim.activity_enter, R.anim.activity_leave);
    }
}
