package com.cybor.paperlessdnd.ui.adapters;


import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.cybor.paperlessdnd.R;
import com.cybor.paperlessdnd.data.Character;
import com.cybor.paperlessdnd.data.InventoryObject;
import com.cybor.paperlessdnd.ui.InventoryActivity;
import com.cybor.paperlessdnd.ui.InventoryObjectActivity;

import java.util.List;

import io.realm.Realm;

import static com.cybor.paperlessdnd.data.InventoryObject.ARMOR;
import static com.cybor.paperlessdnd.data.InventoryObject.SPECIAL_OBJECT;
import static com.cybor.paperlessdnd.data.InventoryObject.TRASH;
import static com.cybor.paperlessdnd.data.InventoryObject.WEAPON;
import static com.cybor.paperlessdnd.ui.InventoryActivity.INVENTORY_OBJECT_REQUEST;
import static com.cybor.paperlessdnd.utils.CommonConstants.POSITION;

public class InventoryListAdapter extends ArrayAdapter
{
    private List<InventoryObject> inventory;
    private InventoryActivity activity;

    public InventoryListAdapter(InventoryActivity activity, List<InventoryObject> inventory)
    {
        super(activity, R.layout.inventory_vh);
        this.inventory = inventory;
        this.activity = activity;
    }

    @NonNull
    @Override
    public View getView(final int position, View view, @NonNull ViewGroup parent)
    {
        if (view == null)
            view = LayoutInflater.from(getContext()).inflate(R.layout.inventory_vh, parent, false);

        if (inventory != null)
        {
            InventoryObject object = inventory.get(position);
            ((TextView) view.findViewById(R.id.number_tv)).setText(String.format("%s. ", position + 1));
            ((TextView) view.findViewById(R.id.name_tv)).setText(object.getName());
            ((TextView) view.findViewById(R.id.count_tv)).setText(String.format("%s", object.getCount()));
            view.findViewById(R.id.menu_button).setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    PopupMenu menu = new PopupMenu(getContext(), view);
                    menu.inflate(R.menu.list_item_menu);
                    menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener()
                    {
                        @Override
                        public boolean onMenuItemClick(MenuItem menuItem)
                        {
                            Context context = getContext();
                            Realm realm = Realm.getDefaultInstance();
                            switch (menuItem.getItemId())
                            {
                                case R.id.edit_item:
                                    activity.startActivityForResult(new Intent(context,
                                                    InventoryObjectActivity.class)
                                                    .putExtra(POSITION, position),
                                            INVENTORY_OBJECT_REQUEST);
                                    break;
                                case R.id.remove_item:
                                    final Character character = Character.getInstance(getContext());
                                    realm.executeTransaction(new Realm.Transaction()
                                    {
                                        @Override
                                        public void execute(Realm realm)
                                        {
                                            character.getInventory().remove(position);
                                        }
                                    });
                                    activity.updateDisplayData();
                                    break;
                            }
                            return true;
                        }
                    });
                    menu.show();
                }
            });

            ImageView objectTypeIV = (ImageView) view.findViewById(R.id.object_type_iv);
            switch (inventory.get(position).getType())
            {
                case TRASH:
                    objectTypeIV.setImageResource(R.mipmap.bag);
                    break;
                case ARMOR:
                    objectTypeIV.setImageResource(R.mipmap.helmet);
                    break;
                case WEAPON:
                    objectTypeIV.setImageResource(R.mipmap.sword);
                    break;
                case SPECIAL_OBJECT:
                    objectTypeIV.setImageResource(R.mipmap.ring);
                    break;
            }
        }
        return view;
    }

    @Override
    public int getCount()
    {
        return inventory == null ? 0 : inventory.size();
    }
}
