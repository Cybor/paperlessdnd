package com.cybor.paperlessdnd.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.cybor.paperlessdnd.R;
import com.cybor.paperlessdnd.data.Character;
import com.cybor.paperlessdnd.data.Skill;
import com.cybor.paperlessdnd.utils.CommonConstants;

import io.realm.Realm;

import static com.cybor.paperlessdnd.utils.CommonConstants.NO_ID;

public class SkillActivity extends AppCompatActivity
        implements View.OnClickListener
{
    private Realm realm;
    private Character character;
    private Skill skill;
    private boolean editMode;
    private TextView nameET, descriptionET, upgradeET, bonusET;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.skill_layout);

        realm = Realm.getDefaultInstance();
        character = Character.getInstance(this);

        int position = getIntent().getIntExtra(CommonConstants.POSITION, NO_ID);
        editMode = position != NO_ID;
        skill = editMode ? character.getSkills().get(position) : new Skill();

        nameET = (TextView) findViewById(R.id.name_et);
        descriptionET = (TextView) findViewById(R.id.description_et);
        upgradeET = (TextView) findViewById(R.id.upgrade_et);
        bonusET = (TextView) findViewById(R.id.bonus_et);

        if (editMode)
        {
            nameET.setText(skill.getName());
            descriptionET.setText(skill.getDescription());
            upgradeET.setText(String.format("%s", skill.getUpgrade()));
            bonusET.setText(String.format("%s", skill.getBonus()));
        }


        View saveButton = findViewById(R.id.save_button);
        View cancelButton = findViewById(R.id.cancel_button);
        assert saveButton != null && cancelButton != null;
        saveButton.setOnClickListener(this);
        cancelButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.save_button:
                realm.executeTransaction(new Realm.Transaction()
                {
                    @Override
                    public void execute(Realm realm)
                    {
                        skill.setName(nameET.getText().toString());
                        skill.setDescription(descriptionET.getText().toString());
                        skill.setUpgrade(Integer.parseInt(upgradeET.getText().toString()));
                        skill.setBonus(Integer.parseInt(bonusET.getText().toString()));
                    }
                });

                if (!editMode)
                    realm.executeTransaction(new Realm.Transaction()
                    {
                        @Override
                        public void execute(Realm realm)
                        {
                            character.getSkills().add(skill);
                        }
                    });
                setResult(RESULT_OK);
                finish();
                break;
            case R.id.remove_button:
                if (editMode)
                    realm.executeTransaction(new Realm.Transaction()
                    {
                        @Override
                        public void execute(Realm realm)
                        {
                            skill.deleteFromRealm();
                        }
                    });
                setResult(RESULT_OK);
                finish();
                break;
            case R.id.cancel_button:
                finish();
                break;
        }
    }

    @Override
    public void finish()
    {
        super.finish();
        overridePendingTransition(R.anim.activity_enter, R.anim.activity_leave);
    }
}
