package com.cybor.paperlessdnd.data;

import android.text.TextUtils;

import org.mariuszgromada.math.mxparser.Constant;
import org.mariuszgromada.math.mxparser.Expression;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.annotations.Ignore;

import static java.lang.Math.floor;

public class Feature extends RealmObject implements Cloneable
{
    private boolean fixed;
    private String name;
    private String formula;
    @Ignore
    private ArrayList<Feature> variables;
    @Ignore
    private Realm realm;

    public Feature()
    {
        realm = Realm.getDefaultInstance();
    }

    public Feature(String name, String formula, boolean fixed)
    {
        this(name, formula, null, fixed);
    }

    public Feature(String name, String formula, ArrayList<Feature> variables, boolean fixed)
    {
        this.name = name;
        this.formula = formula;
        this.variables = variables;
        this.fixed = fixed;
    }

    void addVariable(Feature variable)
    {
        if (variables == null)
            variables = new ArrayList<>();
        if (!variables.contains(variable))
            variables.add(variable);
    }

    public int getValue()
    {
        if (formula.replace(" ", "").isEmpty()) return 0;
        else if (TextUtils.isDigitsOnly(formula)) return Integer.parseInt(formula);
        else
        {
            Expression expression = new Expression(formula);
            if (variables != null)
                for (Feature current : variables)
                    expression.addConstants(new Constant(current.getName(), current.getValue()));
            return (int) floor(expression.calculate());
        }
    }

    boolean isFixed()
    {
        return fixed;
    }

    public void setFixed(final boolean fixed)
    {
        realm.executeTransaction(new Realm.Transaction()
        {
            @Override
            public void execute(Realm realm)
            {
                Feature.this.fixed = fixed;
            }
        });
    }

    public String getName()
    {
        return name;
    }

    public void setName(final String name)
    {
        realm.executeTransaction(new Realm.Transaction()
        {
            @Override
            public void execute(Realm realm)
            {
                Feature.this.name = name;
            }
        });
    }

    public String getFormula()
    {
        return formula;
    }

    public void setFormula(final String formula)
    {
        realm.executeTransaction(new Realm.Transaction()
        {
            @Override
            public void execute(Realm realm)
            {
                Feature.this.formula = formula;
            }
        });
    }

    //Ignored
    public List<Feature> getVariables()
    {
        return variables;
    }

}
