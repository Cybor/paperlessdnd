package com.cybor.paperlessdnd.data;

import io.realm.RealmObject;

public class Spell extends RealmObject
{
    private int power, level, modifier;
    private String name, description;

    public Spell()
    {

    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public int getPower()
    {
        return power;
    }

    public void setPower(int power)
    {
        this.power = power;
    }

    public int getLevel()
    {
        return level;
    }

    public void setLevel(int level)
    {
        this.level = level;
    }

    public int getModifier()
    {
        return modifier;
    }

    public void setModifier(int modifier)
    {
        this.modifier = modifier;
    }
}
