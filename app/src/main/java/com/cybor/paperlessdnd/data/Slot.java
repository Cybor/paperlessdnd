package com.cybor.paperlessdnd.data;

import io.realm.RealmObject;

public class Slot extends RealmObject
{
    private String name;
    private InventoryObject inventoryObject;

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public InventoryObject getInventoryObject()
    {
        return inventoryObject;
    }

    public void setInventoryObject(InventoryObject inventoryObject)
    {
        this.inventoryObject = inventoryObject;
    }

    public int getType()
    {
        return inventoryObject.getType();
    }

    @Override
    public String toString()
    {
        return name;
    }
}
