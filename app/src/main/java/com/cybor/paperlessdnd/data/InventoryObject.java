package com.cybor.paperlessdnd.data;


import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;

public class InventoryObject extends RealmObject
{
    public static final int TRASH = 0, ARMOR = 1, WEAPON = 2, SPECIAL_OBJECT = 3;
    private int count, type;
    private String name, description;
    private RealmList<Feature> specifications;

    public InventoryObject()
    {
        specifications = new RealmList<>();
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public int getCount()
    {
        return count;
    }

    public void setCount(int count)
    {
        this.count = count;
    }

    public int getType()
    {
        return type;
    }

    public void setType(int type)
    {
        this.type = type;
    }

    public List<Feature> getSpecifications()
    {
        return specifications;
    }

    @Override
    public String toString()
    {
        return name;
    }
}
