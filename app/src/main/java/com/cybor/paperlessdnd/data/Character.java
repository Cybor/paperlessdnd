package com.cybor.paperlessdnd.data;

import android.content.Context;

import com.cybor.paperlessdnd.R;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.Ignore;

public class Character extends RealmObject
{
    private static Character instance;
    private int level, age;
    private boolean gender;
    private String name;
    private String cClass;
    private String race;
    private String ideology;
    private String god;
    private String personalNotes;
    private RealmList<Feature> spellsCounts;
    private RealmList<Feature> characteristics;
    private RealmList<Feature> distinctiveFeatures;
    private RealmList<Skill> skills;
    private RealmList<Spell> spells;
    private RealmList<InventoryObject> inventory;
    private RealmList<InventoryObject> specialObjects;
    private RealmList<Slot> slots;
    @Ignore
    private Realm realm;

    //region Constructor
    public Character()
    {
        spellsCounts = new RealmList<>();
        characteristics = new RealmList<>();
        distinctiveFeatures = new RealmList<>();
        skills = new RealmList<>();
        spells = new RealmList<>();
        inventory = new RealmList<>();
        specialObjects = new RealmList<>();
        slots = new RealmList<>();
    }

    //endregion
    //region Singleton pattern implementation
    public static Character getInstance(Context context)
    {
        return getInstance(context, false);
    }

    public static Character getInstance(final Context context, boolean reload)
    {
        Realm realm = Realm.getDefaultInstance();
        if (instance == null || reload)
        {
            Character character = realm.where(Character.class).findFirst();
            if (character == null)
            {
                instance = new Character();
                for (final String current : context
                        .getResources()
                        .getStringArray(R.array.fixed_characteristics))
                    realm.executeTransaction(new Realm.Transaction()
                    {
                        @Override
                        public void execute(Realm realm)
                        {
                            instance.characteristics.add(new Feature(current, "", true));
                        }
                    });
                realm.executeTransaction(new Realm.Transaction()
                {
                    @Override
                    public void execute(Realm realm)
                    {
                        instance = realm.copyToRealm(instance);
                    }
                });
            } else instance = character;
        }
        instance.realm = realm;
        return instance;
    }

    //endregion
    //region Level
    public int getLevel()
    {
        return level;
    }

    public void setLevel(final int level)
    {
        realm.executeTransaction(new Realm.Transaction()
        {
            @Override
            public void execute(Realm realm)
            {
                Character.this.level = level;
            }
        });
    }

    //endregion
    //region Age
    public int getAge()
    {
        return age;
    }

    public void setAge(final int age)
    {
        realm.executeTransaction(new Realm.Transaction()
        {
            @Override
            public void execute(Realm realm)
            {
                Character.this.age = age;
            }
        });
    }

    //endregion
    //region Gender
    public boolean getGender()
    {
        return gender;
    }

    public void setGender(final boolean gender)
    {
        realm.executeTransaction(new Realm.Transaction()
        {
            @Override
            public void execute(Realm realm)
            {
                Character.this.gender = gender;
            }
        });
    }

    //endregion
    //region Name
    public String getName()
    {
        return name;
    }

    public void setName(final String name)
    {
        realm.executeTransaction(new Realm.Transaction()
        {
            @Override
            public void execute(Realm realm)
            {
                Character.this.name = name;
            }
        });
    }

    //endregion
    //region CharacterClass
    public String getCharacterClass()
    {
        return cClass;
    }

    public void setClass(final String cClass)
    {
        realm.executeTransaction(new Realm.Transaction()
        {
            @Override
            public void execute(Realm realm)
            {
                Character.this.cClass = cClass;
            }
        });
    }

    //endregion
    //region Race
    public String getRace()
    {
        return race;
    }

    public void setRace(final String race)
    {
        realm.executeTransaction(new Realm.Transaction()
        {
            @Override
            public void execute(Realm realm)
            {
                Character.this.race = race;
            }
        });
    }

    //endregion
    //region Ideology
    public String getIdeology()
    {
        return ideology;
    }

    public void setIdeology(final String ideology)
    {
        realm.executeTransaction(new Realm.Transaction()
        {
            @Override
            public void execute(Realm realm)
            {
                Character.this.ideology = ideology;
            }
        });
    }

    //endregion
    //region God
    public String getGod()
    {
        return god;
    }

    public void setGod(final String god)
    {
        realm.executeTransaction(new Realm.Transaction()
        {
            @Override
            public void execute(Realm realm)
            {
                Character.this.god = god;
            }
        });
    }

    //endregion
    //region PersonalNotes
    public String getPersonalNotes()
    {
        return personalNotes;
    }

    public void setPersonalNotes(final String personalNotes)
    {
        realm.executeTransaction(new Realm.Transaction()
        {
            @Override
            public void execute(Realm realm)
            {
                Character.this.personalNotes = personalNotes;
            }
        });
    }

    //endregion
    //region Collections
    //region Characteristics
    public List<Feature> getFixedCharacteristics()
    {
        return getFixedCharacteristics(true, false);
    }

    public List<Feature> getDynamicCharacteristics(boolean includeVariables)
    {
        return getFixedCharacteristics(false, includeVariables);
    }

    private List<Feature> getFixedCharacteristics(boolean fixed, boolean includeVariables)
    {
        List<Feature> result = new ArrayList<>();
        List<Feature> source = getCharacteristics();

        for (Feature current : source)
            if (current.isFixed() == fixed)
            {
                if (!fixed && includeVariables)
                    for (Feature variable : source)
                        if (variable.isFixed())
                            current.addVariable(variable);
                result.add(current);
            }
        return result;
    }

    //endregion

    public RealmList<Slot> getSlots()
    {
        return slots;
    }

    public RealmList<Feature> getDistinctiveFeatures()
    {
        return distinctiveFeatures;
    }

    public RealmList<Feature> getSpellsCounts()
    {
        return spellsCounts;
    }

    public RealmList<Feature> getCharacteristics()
    {
        return characteristics;
    }

    public RealmList<Skill> getSkills()
    {
        return skills;
    }

    public RealmList<Spell> getSpells()
    {
        return spells;
    }

    public RealmList<InventoryObject> getInventory()
    {
        return inventory;
    }

    public RealmList<InventoryObject> getSpecialObjects()
    {
        return specialObjects;
    }
    //endregion
}
