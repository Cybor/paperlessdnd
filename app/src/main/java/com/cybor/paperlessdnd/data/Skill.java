package com.cybor.paperlessdnd.data;


import io.realm.RealmObject;

public class Skill extends RealmObject
{
    private int upgrade, bonus;
    private String name, description;

    public Skill()
    {

    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public int getUpgrade()
    {
        return upgrade;
    }

    public void setUpgrade(int upgrade)
    {
        this.upgrade = upgrade;
    }

    public int getBonus()
    {
        return bonus;
    }

    public void setBonus(int bonus)
    {
        this.bonus = bonus;
    }
}